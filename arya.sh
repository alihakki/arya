#!/bin/bash

cols=$(tput cols)
lines=$(tput lines)

loadkeys trq
setfont eurlatgr

turkish(){
main_menu_Title='Ana Menü'
main_menu_Description='Bu program arch kurulumu için seçenekli bir script yapısı için temel oluşturmayı amaçlamaktadır.\n Script dosyasının içeriğinde kendinize özgü değişiklikler yapmanızı öneririm.'

KeymapManager='Klavye Düzeni'
KeymapManager_Des='Klavye düzeninizi seçin.'
keymap_manager_Title=$KeymapManager
keymap_manager_Des=$KeymapManager_Des

HostnameManager='Bilgisayar Adı'
HostnameManager_Des='Bilgisayarınza bir ad verin.'
hostname_manager_Title=$HostnameManager
hostname_manager_Des=$HostnameManager_Des

MirrorManager='Yansı Yöneticisi'
MirrorManager_Des='Yansılarınızı düzenleyin.'
MirrorManager_Changes_Title='Yansı Değişikliği Uyarısı'
MirrorManager_Changes_Des='Aşağıdaki seçili yansıları değiştirmek istiyormusnuz.'
mirror_manager_Title=$MirrorManager
mirror_manager_Des='Bu işlem kurulumdan hemen önce çalıştırılır.\nPaketlerin en hızlı yansıdan çekilmesini sağlar.'
RankMirrors='Tüm yansılar'
RankMirrors_Des='Yansıları erişim hızana göre sıralar. (3-5dk sürebilir)'
SpecificMirrors='Sadece seçili yansılar'
SpecificMirrors_Des='Seçili yansıları erişim hızına göre sıralar.'
select_mirrorlist_Title='Yansı Listesi'
select_mirrorlist_Des='Kullanmak istediğiniz yansıları seçiniz.'


DiskManager='Disk Yöneticisi'
DiskManager_Des='Disklerinizi düzenleyin.'
disk_manager_Title=$DiskManager
disk_manager_Des='Düzenlemek için disk yada bölüm seçin.\n!!!Diskler üzerinde anında işlem yapılır!!!\nDisk bağlama işlemi bittiğinde Cancel ile Ana menüye geçiş yapın ve bir sonraki adıma geçin.'
partition_manager_Title='Disk Bölümleyici'
partition_manager_Des='Diski bölümlerini düzenlemek için bir program seçin.'
format_manager_Title='Bölüm Biçimlendirici'
format_manager_Des='Disk bölümünü biçimlendirin.'
format_manager_none_Des='Bu bölümü biçimlendirme.'
format_manager_fat32_Des='efi başlangıç bölümü olarak biçimlendir.'
format_manager_ext4_Des='normal linux ext4 bölümü olarak biçimlendir. '
format_manager_swap_Des='takas alanı olarak biçimlendir.'



mount_manager_Title='Bağlama Yöneticisi'
mount_manager_Des='Disk bölümünü bağlamak istediğiniz dizini seçin.'
mount_manager_none_Des='Disk bölümünü bağlama.'
mount_manager_efi_Des='EFI Açılış bölümü olarak bağla.'
mount_manager_mnt_Des='Sistemin kurulacağı bölüm olarak bağla.'
mount_manager_boot_Des='Sistem yükleyici bölümü olarak bağla.'
mount_manager_home_Des='Kullanıcılar için ev dizini olarak bağla.'
mount_manager_Data_Des='Verilerini sakladığın disk bölümü için.'
mount_manager_swap_Des='Takas alanı olarak bağla'


BootManager='Açılış Yöneticisi'
BootManager_Des='Açılış yöneticinizi seçin.'
boot_manager_Title=$BootManager
boot_manager_Des=$BootManager_Des
boot_manager_Changes_Title='Açılış Yöneticisi Değişikliği Uyarısı'
boot_manager_Changes_Des='Açılş yöneticisini değiştirmek istiyormusunuz?'

UserManager='Kullanıcı Yöneticisi'
UserManager_Des='Bir kullanıcı oluşturun.'
user_manager_Title=$UserManager
root_check_Changes_Title='root Şifre Değişikliği'
root_check_Changes_Des='root şifresi zaten belirlenmiş değiştirmek istiyormusunuz?'
ask_root_pass_Title='Root Şifresi'
ask_root_pass_Des='root için bir şifre giriniz:'
ask_root_repass_Des='root için şifreyi tekrar giriniz:'
user_check_Changes_Title='Kullanıcı Değişikliği'
user_check_Changes_Des='kullanıcısı zaten belirlenmiş değiştirmek istiyormusunuz?'
get_userinfo_Title='Kullanıcı Bilgi_Girişi'
get_userinfo_namefull_Des='Kullanıcının tam adını giriniz: '
get_userinfo_name_Des='Kullanıcı adı giriniz:'
ask_user_pass_Title='Kullanıcı Şifresi'
ask_user_pass_Des='Kullanıcısı için bir şifre giriniz:'
ask_user_repass_Des='Kullanıcısı için şifreyi tekrar giriniz:'

get_userinfo_wheel_Des='Kullanıcısı yönetici yetkilerine sahip olsunmu.'


DriverManager='Sürücü Yöneticisi'
DriverManager_Des='Donanımınıza uygun sürücüleri seçin.'
driver_manager_Changes_Title='Seçili Sürücü Değişikliği'
driver_manager_Changes_Des='Aşağıdaki Seçili Sürücüleri Değiştirmek istiyormusunuz.'
select_drivers_Title='Sürücü Seçimi'
select_drivers_Des='Yanlış sürücü seçimi sisteminizde kararsızlıklara neden olabilir.\nEğer emin değilseniz lütfen forum sitelerinde destek arayın.'
select_drivers_pulseaudio_Des='Alsa ses sürücüsü ile birlikte pulseaudio ses yöneticisi'
select_drivers_networkmanager_Des='Ağ yöneticisi'
select_drivers_amdgpu_Des='AMD ekran kartı sürücüsü'
select_drivers_intel_Des='Intel ekran kartı sürücüsü'
select_drivers_nouveau_Des='Nvidia için açık kaynak sürücü'
select_drivers_nvidia_Des='Nvidia sahipli sürücü'
select_drivers_cups_Des='Yazıcı cihaz desteği'
select_drivers_hplip_Des='HP yazıcı sürücüsü (cups_bağımlılığı_vardır)'
select_drivers_trim_Des='Sabit diskler için trim özelliğini aktifleştirir.'
select_drivers_vboxguest_Des='virtualbox sanal makineleri için sürücü.'

DesktopManager='Masaüstü Yöneticisi'
DesktopManager_Des='İstediğiniz masaüstünü seçin.'
desktop_manager_Changes_Title='Seçili masaüstü değişikliği'
desktop_manager_Changes_Des='Aşağıdaki Seçili Masaüstü Yöneticilerini Değiştirmek istiyormusunuz'
select_desktops_Title='Masaüstü Seçici'
select_desktops_Des='Kullanmak istediğiniz masaüstü yöneticisini seçiniz'
select_desktops_Gnome_Des='Gnome masaüstü (tanrı seni sol üst köşeden korusun)'
select_desktops_Plasma_Des='Plasma5 masaüstü (Sadece kate için bile yüklenir)'
select_desktops_XFCE_Des='XFCE4 masaüstü (Güzel bir ortam)'

ApplicationManager='Program Yöneticisi'
ApplicationManager_Des='YÜklenmesini istediğiniz programlar.'
program_manager_Changes_Title='Seçili program değişikliği'
program_manager_Changes_Des='Aşağıdaki Seçili Programları Değiştirmek istiyormusunuz'
select_programs_Title='Program Seçici'
select_programs_Des='YÜklenmesini istediğiniz programları Seçiniz.'
select_programs_Blender_Des='3D modelleme animasyon programı'
select_programs_Digikam_Des='Fotoğraf arşivi yöneticisi'
select_programs_Firefox_Des='İnternet tarayıcı'
select_programs_Opera_Des='Ücretsiz VPN destekli internet tarayıcı'
select_programs_Libreoffice_Des='Ofis programı'
select_programs_Steam_Des='Steam oyun platformu'
select_programs_Cantata_Des='MPD destekli müzik oynatıcı'
select_programs_Virtualbox_Des='Sanal bilgisayar yöneticisi'


check_install_Title='Yükleme Kontrolcüsü'
check_install_Rankmirrors_Des='Yansı listesi ayarlanmadı'
check_install_RootDisk_Des='/mnt dizini bağlanmadı'
check_install_selected_boot_Des='Başlangış yöneticisi seçilmedi'
check_install_username_Des='Bir kullanıcı oluşturulmadı'
check_install_user_password_Des='Kullanıcı şifresi belirlenmedi'
check_install_root_password_Des='root şifresi belirlenmedi'

InstallationStarter='Yükleme Başalatıcısı'
InstallationStarter_Des='Yüklemeyi başlatın.'
start_install_Title="$InstallationStarter"
start_install_Des='Bak kuruyorum, sonra geri dönüşü yok bu işin.'

}

turkish

backup_mirrorlist(){
if [ ! -f "/etc/pacman.d/mirrorlist.backup" ]; then
	if [ -f "/etc/pacman.d/mirrorlist.pacnew" ]; then
        cp "/etc/pacman.d/mirrorlist.pacnew" "/etc/pacman.d/mirrorlist.backup"
        
    else
        cp "/etc/pacman.d/mirrorlist" "/etc/pacman.d/mirrorlist.backup"
    fi
fi
}

backup_mirrorlist

main_menu(){
    selected_menu=$(whiptail --title "$main_menu_Title" --menu "$main_menu_Description" $lines $cols 16 \
    "$KeymapManager"        "$KeymapManager_Des" \
    "$HostnameManager"       "$HostnameManager_Des" \
    "$MirrorManager"         "$MirrorManager_Des" \
    "$DiskManager"           "$DiskManager_Des" \
    "$BootManager"           "$BootManager_Des" \
    "$UserManager"           "$UserManager_Des" \
    "$DriverManager"         "$DriverManager_Des" \
    "$DesktopManager"        "$DesktopManager_Des" \
    "$ApplicationManager"    "$ApplicationManager_Des" \
    "$InstallationStarter"   "$InstallationStarter_Des" 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus = 0 ]; then
        case $selected_menu in
        "$KeymapManager" )
            keymap_manager ;;
        "$HostnameManager" )
            hostname_manager ;;
        "$MirrorManager" )
            mirror_manager ;;
        "$DiskManager" )
            disk_manager ;;
        "$BootManager" )
            boot_manager ;;
        "$UserManager" )
            user_manager ;;
        "$DriverManager" )
            driver_manager ;;
        "$DesktopManager" )
            desktop_manager ;;
        "$ApplicationManager" )
            program_manager ;;
        "$InstallationStarter" )
            check_install ;;
        esac
    else
        umount -R /mnt
        swapoff -a
        exit
    fi
}

keymap_manager(){
    selected_keymap=$(whiptail --title "$keymap_manager_Title" --radiolist "$keymap_manager_Des"  $lines $cols 15 \
        "us" "" off \
        "trq" "" on 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [[ $exitstatus = 0 ]]; then
        loadkeys "$selected_keymap"
    fi
    main_menu
}

select_mirrorlist(){
    selected_countries=($(whiptail --title "$select_mirrorlist_Title" --checklist --separate-output "$select_mirrorlist_Des" $lines $cols 15 \
        "AU" "Australia" off \
        "AT" "Austria" off \
        "BY" "Belarus" off \
        "BE" "Belgium" off \
        "BR" "Brazil" off \
        "BG" "Bulgaria" off \
        "CA" "Canada" off \
        "CL" "Chile" off \
        "CN" "China" off \
        "CO" "Colombia" off \
        "CZ" "Czech Republic" off \
        "DK" "Denmark" off \
        "EE" "Estonia" off \
        "FI" "Finland" off \
        "FR" "France" off \
        "DE" "Germany" off \
        "GR" "Greece" off \
        "HK" "Hong Kong" off \
        "HU" "Hungary" off \
        "ID" "Indonesia" off \
        "IN" "India" off \
        "IR" "Iran" off \
        "IE" "Ireland" off \
        "IL" "Israel" off \
        "IT" "Italy" off \
        "JP" "Japan" off \
        "KZ" "Kazakhstan" off \
        "KR" "Korea" off \
        "LV" "Latvia" off \
        "LU" "Luxembourg" off \
        "MK" "Macedonia" off \
        "NL" "Netherlands" off \
        "NC" "New Caledonia" off \
        "NZ" "New Zealand" off \
        "NO" "Norway" off \
        "PL" "Poland" off \
        "PT" "Portugal" off \
        "RO" "Romania" off \
        "RU" "Russia" off \
        "RS" "Serbia" off \
        "SG" "Singapore" off \
        "SK" "Slovakia" off \
        "ZA" "South Africa" off \
        "ES" "Spain" off \
        "LK" "Sri Lanka" off \
        "SE" "Sweden" off \
        "CH" "Switzerland" off \
        "TW" "Taiwan" off \
        "TR" "Turkey" off \
        "UA" "Ukraine" off \
        "GB" "United Kingdom" off \
        "US" "United States" off \
        "UZ" "Uzbekistan" off \
        3>&1 1>&2 2>&3))
}


mirror_manager(){
    selected_mirrorlist_method=$(whiptail --title "$mirror_manager_Title" --radiolist "$mirror_manager_Des" $lines $cols 16 \
    "$RankMirrors" "$RankMirrors_Des" on \
    "$SpecificMirrors" "$SpecificMirrors_Des" off 3>&1 1>&2 2>&3)
    exitstatus=$?

    if [ $exitstatus = 0 ]; then
        case $selected_mirrorlist_method in
        $RankMirrors )
            Rankmirrors='true'
            Specificmirrors='false'
            ;;
        $SpecificMirrors )
            Specificmirrors='true'
            Rankmirrors='false'
            if [[ ! -z ${selected_countries[@]} ]]; then
                mirror_manager_Changes_Message="$MirrorManager_Changes_Des \n ${selected_countries[@]}"
                if (whiptail --title "$MirrorManager_Changes_Title" --yesno "$mirror_manager_Changes_Message" $lines $cols) then
                    select_mirrorlist
                fi
            else
                select_mirrorlist
            fi
            ;;
        esac
	fi
    main_menu
}

set_mirrorlist(){
	pacman -Sy
    pacman -S --noconfirm pacman-mirrorlist
    pacman -S --noconfirm pacman-contrib

    if [[ $Rankmirrors = "true" ]]; then
    	rankmirrors "/etc/pacman.d/mirrorlist.backup" > "/etc/pacman.d/mirrorlist.new"
    fi

    if [[ $Specificmirrors = "true" ]]; then
        for selected_country in "${selected_countries[@]}"; do

            mirror_gen_url="https://www.archlinux.org/mirrorlist/?country=$selected_country&use_mirror_status=on"

            if [ ! -f "/etc/pacman.d/mirrorlist.countries" ]; then
                curl $mirror_gen_url > "/etc/pacman.d/mirrorlist.countries"
            else
                curl $mirror_gen_url | sed -n '5,$p' >> "/etc/pacman.d/mirrorlist.countries"
            fi
        done
    
        rankmirrors "/etc/pacman.d/mirrorlist.countries" > "/etc/pacman.d/mirrorlist.new"
        
        sed -i 's/^#Server/Server/' "/etc/pacman.d/mirrorlist.new"
    fi
    
    rm "/etc/pacman.d/mirrorlist"
    rm "/etc/pacman.d/mirrorlist.pacnew"
    mv "/etc/pacman.d/mirrorlist.new" "/etc/pacman.d/mirrorlist"
}


disk_manager(){
    unset disk_list
    i=0
    disks=($(lsblk -io KNAME | grep 'sd[        cp "/etc/pacman.d/mirrorlist" "/etc/pacman.d/mirrorlist.backup"a-z]'))
    for disk in ${disks[@]} ;do
        disk_list[i]="/dev/"$disk
        type="$(lsblk --noheadings -d -o TYPE,FSTYPE,SIZE,LABEL,MODEL,MOUNTPOINT "${disk_list[i]}")"
        i=$((i+1))
        disk_list[i]=$type
        i=$((i+1))
    done
    
    selected_disk=$(whiptail --title "$disk_manager_Title" --menu "$disk_manager_Des" $lines $cols 16 "${disk_list[@]}" 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus = 0 ]; then
        selected_disk_type="$(lsblk --noheadings -d -o TYPE "$selected_disk")"
        if [ $selected_disk_type = "disk" ]; then
            partition_manager
        else
            selected_partition=$selected_disk
            format_manager
        fi
    fi
    main_menu
}

partition_manager(){
    selected_disk_manager=$(whiptail --title "$partition_manager_Title" --menu "$selected_disk $partition_manager_Des" $lines $cols 16 \
    "cfdisk" "for MBR and GPT partitions" \
    "cgdisk" "for GPT partitions" 3>&1 1>&2 2>&3)
    exitstatus=$?
    
    if [ $exitstatus = 0 ]; then
        case $selected_disk_manager in
        "cfdisk" )
            cfdisk $selected_disk;;
        "cgdisk" )
            cgdisk $selected_disk;;
        esac
    fi
    disk_manager
}

format_manager(){
    selected_format=$(whiptail --title "$format_manager_Title" --menu "$selected_disk $format_manager_Des" $lines $cols 16 \
    "none" "$format_manager_none_Des" \
    "fat32" "$format_manager_fat32_Des" \
    "ext4" "$format_manager_ext4_Des" \
    "swap" "$format_manager_swap_Des" 3>&1 1>&2 2>&3)
    exitstatus=$?
    
    if [ $exitstatus = 0 ]; then
        case $selected_format in
        "none" )
            ;;
        "fat32" )
            mkfs.fat -F32 $selected_partition;;
        "ext4" )
            mkfs.ext4 -F $selected_partition;;
        "swap" )
            mkswap -f $selected_partition;;
        esac
        mount_manager
    fi
    disk_manager
}

check_mounted_fs(){
echo $(lsblk -l | grep "$1$" | awk '{print "/dev/" $1}')
}

mount_manager(){
    if [ -z $mntcheck ]; then
        mntcheck=0
    fi

    RootDisk=$(check_mounted_fs "/mnt")
    mounted_dir="$(lsblk --noheadings -d -o MOUNTPOINT "$selected_partition")"

    selected_dir=$(whiptail --title "$mount_manager_Title" --menu "$selected_partition $mount_manager_Des" $lines $cols 16 \
    "none" "$mount_manager_none_Des" \
    "/mnt" "$mount_manager_mnt_Des" \
    "/mnt/boot/efi" "$mount_manager_efi_Des" \
    "/mnt/boot" "$mount_manager_boot_Des" \
    "/mnt/home" "$mount_manager_home_Des" \
    "/mnt/mnt/Data" "$mount_manager_Data_Des" \
    "[SWAP]" "$mount_manager_swap_Des" 3>&1 1>&2 2>&3)
    exitstatus=$?
                    
    if [ $exitstatus = 0 ]; then
        if [[ ! -z $mounted_dir ]] && [[ $selected_dir="none" ]]; then
        case $mounted_dir in
            "/mnt" )
                umount -R $mounted_dir
                mntcheck=0
                ;;
            "[SWAP]" )
                swapoff $selected_partition
                ;;
            * )
                umount -R $mounted_dir
                ;;
        esac
        fi
            
        if [ $mntcheck = 0 ]; then
            if [ $selected_dir = "/mnt" ] ; then
                mkdir -p $selected_dir
                mount $selected_partition $selected_dir
                mntcheck=1
            else
                whiptail --title "mnt kontrol" --msgbox "lütfen önce mnt dizinini bağlayın" $lines $cols
            fi
        else
            case $selected_dir in
                "none" )
                    ;;
                "[SWAP]" )
                    swapon $selected_partition ;;
                * )
                    mkdir -p $selected_dir
                    mount $selected_partition $selected_dir ;;
            esac
        fi
    fi
}

boot_manager(){
    RootDisk=$(check_mounted_fs "/mnt")
    BootDisk=$(check_mounted_fs "/mnt/boot")
    if [[ -z $BootDisk ]]; then
        BootDisk=$RootDisk
    fi
    
    if [[ ! -z $selected_boot ]]; then
        if (whiptail --title "$boot_manager_Changes_Title" --yesno "$selected_boot $boot_manager_Changes_Des" $lines $cols) then
            select_boot="true"
        else
            select_boot="false"
        fi
    fi
    
    if ($select_boot) then
        selected_boot=$(whiptail --title "$boot_manager_Title" --radiolist "$boot_manager_Des" $lines $cols 16 \
        "Uefi" "The motherboard should be uefi supported." off \
        "Grub" "Legacy boot method" on 3>&1 1>&2 2>&3)
        exitstatus=$?
    
        if [ $exitstatus = 0 ]; then
            boot_uefi="false" boot_grub="false"
            case $selected_boot in
            "Uefi" )
                boot_uefi="true"
                ;;
            "Grub" )
                boot_grub="true"
                ;;
            esac
        fi
    fi
    
main_menu
}

set_boot(){
    if [[ $boot_uefi = "true" ]]; then
        rm /mnt/boot/loader/loader.conf
        arch-chroot /mnt bootctl install
        RootDiskUUID=$(blkid -s PARTUUID -o value $RootDisk)
        echo "title Arch Linux" > /mnt/boot/loader/entries/arch.conf
        echo "linux /vmlinuz-linux" >> /mnt/boot/loader/entries/arch.conf
        echo "initrd /initramfs-linux.img" >> /mnt/boot/loader/entries/arch.conf
        echo "options root=PARTUUID=$RootDiskUUID rw" >> /mnt/boot/loader/entries/arch.conf
    fi
            
    if [[ $boot_grub = "true" ]]; then
        ueficheck=$(cat /sys/firmware/efi/fw_platform_size)
        arch-chroot /mnt pacman -S --noconfirm grub efibootmgr
        BootDisk=$(echo $BootDisk | sed s'/.$//')
        if [[ $ueficheck = 64 ]]; then
            arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
        else
            arch-chroot /mnt grub-install --recheck $BootDisk
        fi
        arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
    fi
}

user_manager(){
root_check
user_check
main_menu
}

password_menu(){
title=$1
message=$2
password=$(whiptail --title "$title" --passwordbox "$message" $lines $cols  3>&1 1>&2 2>&3)
echo "$password"
}

root_check(){
    if [[ ! -z $root_password ]]; then
        if (whiptail --title "$root_check_Changes_Title" --yesno "$root_check_Changes_Des" $lines $cols) then
            while :
            do
                root_password=$(password_menu "$ask_root_pass_Title" "$ask_root_pass_Des" )    
                root_password_check=$(password_menu "$ask_root_pass_Title" "$ask_root_repass_Des" )
                if [[ "$root_password" = "$root_password_check" ]] && [[ ! -z $root_password ]]; then
                    break
                fi
            done
        fi
    else    
            while :
            do
                root_password=$(password_menu "$ask_root_pass_Title" "$ask_root_pass_Des" )    
                root_password_check=$(password_menu "$ask_root_pass_Title" "$ask_root_repass_Des" )
                if [[ "$root_password" = "$root_password_check" ]] && [[ ! -z $root_password ]]; then
                    break
                fi
            done
    fi
}

user_check(){
    if [[ ! -z $username ]]; then
        if (whiptail --title "$user_check_Changes_Title" --yesno "$username $user_check_Changes_Des" $lines $cols) then
            get_userinfo
        fi
    else
    get_userinfo
    fi    
}

get_userinfo(){
usernamefull=$(whiptail --title "$get_userinfo_Title" --inputbox "$get_userinfo_namefull_Des" $lines $cols  3>&1 1>&2 2>&3)
username=$(whiptail --title "$get_userinfo_Title" --inputbox "$get_userinfo_name_Des" $lines $cols  3>&1 1>&2 2>&3)
exitstatus=$?
if [ $exitstatus = 0 ]; then
    while :
    do
        user_password=$(password_menu "$ask_user_pass_Title" "$username $ask_user_pass_Des" )    
        user_password_check=$(password_menu "$ask_user_pass_Title" "$username $ask_user_repass_Des" )  
        if [[ "$user_password" = "$user_password_check" ]] && [[ ! -z $user_password ]]; then
            break
        fi
        
    done

    if (whiptail --title "$get_userinfo_Title" --yesno "$username $get_userinfo_wheel_Des" $lines $cols) then
        wheel_user="true"
    else
        wheel_user="false"
    fi
fi
}

set_user(){
    arch-chroot /mnt useradd -m -c "$usernamefull" $username
    
    if [[ $wheel_user = "true" ]]; then
        arch-chroot /mnt usermod -aG wheel $username
    fi
    
    echo -e "$user_password\n$user_password" | arch-chroot /mnt passwd $username
    echo -e "$root_password\n$root_password" | arch-chroot /mnt passwd root
}

driver_manager(){
    driver_manager_Changes_Message="$driver_manager_Changes_Des \n ${selected_drivers[@]}"
    if [[ ! -z ${selected_drivers[@]} ]]; then
        if (whiptail --title "$driver_manager_Changes_Title" --yesno "$driver_manager_Changes_Message" $lines $cols) then
            unset selected_drivers
            select_drivers
        fi
    else
        select_drivers
    fi
    main_menu
}

select_drivers(){
    selected_drivers=($(whiptail --title "$select_drivers_Title" --checklist --separate-output "$select_drivers_Des" $lines $cols 16 \
    "pulseaudio" "$select_drivers_pulseaudio_Des" on \
    "networkmanager" "$select_drivers_networkmanager_Des" on \
    "amdgpu" "$select_drivers_amdgpu_Des" off \
    "intel" "$select_drivers_intel_Des" off \
    "nouveau" "$select_drivers_nouveau_Des" off \
    "nvidia" "$select_drivers_nvidia_Des" off \
    "cups" "$select_drivers_cups_Des" on \
    "hplip" "$select_drivers_hplip_Des" off \
    "trim" "$select_drivers_trim_Des" on \
    "vboxguest" "$select_drivers_vboxguest_Des" off 3>&1 1>&2 2>&3))
}

install_drivers(){
    for driver in "${selected_drivers[@]}"; do
        case "$driver" in
        "pulseaudio")
            arch-chroot /mnt pacman -S --noconfirm alsa-utils pulseaudio-alsa
            ;;
        "networkmanager")
            arch-chroot /mnt pacman -S --noconfirm networkmanager
            arch-chroot /mnt systemctl enable NetworkManager.service
            ;;
        "amdgpu")
            arch-chroot /mnt pacman -S --noconfirm xf86-video-amdgpu vulkan-radeon mesa-libgl lib32-mesa-libgl mesa-vdpau libva-mesa-driver
            ;;
        "intel")
            arch-chroot /mnt pacman -S --noconfirm xf86-video-intel vulkan-intel mesa-libgl lib32-mesa-libgl libva-intel-driver libvdpau-va-gl
            ;;
        "nouveau")
            arch-chroot /mnt pacman -S --noconfirm xf86-video-nouvea mesa-libgl lib32-mesa-libgl mesa-vdpau libva-vdpau-driver
            ;;
        "nvidia")
            arch-chroot /mnt pacman -S --noconfirm nvidia nvidia-libgl lib32-nvidia-libgl libva-vdpau-driver
            ;;
        "cups")
            arch-chroot /mnt pacman -S --noconfirm cups
            arch-chroot /mnt systemctl enable org.cups.cupsd.service
            ;;
        "hplip")
            arch-chroot /mnt pacman -S --noconfirm hplip python-gobject python-pyqt5 wget
            ;;
        "trim")
            arch-chroot /mnt systemctl enable fstrim.timer
            ;;
        "vboxguest")
            arch-chroot /mnt pacman -S --noconfirm virtualbox-guest-modules-arch
            ;;
        esac
    done
}

program_manager(){
    program_manager_Changes_Message="$program_manager_Changes_Des \n ${selected_programs[@]}"
    if [[ ! -z ${selected_programs[@]} ]]; then
        if (whiptail --title "$program_manager_Changes_Title" --yesno "$program_manager_Changes_Message" $lines $cols) then
            unset selected_programs
            select_programs
        fi
    else
        select_programs
    fi
    main_menu
}

select_programs(){
    selected_programs=($(whiptail --title "$select_programs_Title" --checklist --separate-output "$select_programs_Des" $lines $cols 16 \
    "Blender" "$select_programs_Blender_Des" off \
    "Digikam" "$select_programs_Digikam_Des" off \
    "Firefox" "$select_programs_Firefox_Des" off \
    "Opera" "$select_programs_Opera_Des" off \
    "Libreoffice" "$select_programs_Libreoffice_Des" off \
    "Steam" "$select_programs_Steam_Des" off \
    "Cantata" "$select_programs_Cantata_Des" off \
    "Virtualbox" "$select_programs_Virtualbox_Des" off 3>&1 1>&2 2>&3))
}

install_programs(){
    for driver in "${selected_programs[@]}"; do
        case "$driver" in
        "Blender")
            arch-chroot /mnt pacman -S --noconfirm blender
            ;;
        "Digikam")
            arch-chroot /mnt pacman -S --noconfirm digikam
            ;;
        "Firefox")
            arch-chroot /mnt pacman -S --noconfirm firefox
            ;;
        "Opera")
            arch-chroot /mnt pacman -S --noconfirm opera opera-ffmpeg-codecs
            ;;
        "Libreoffice")
            arch-chroot /mnt pacman -S --noconfirm libreoffice-fresh
            ;;
        "Steam")
            arch-chroot /mnt pacman -S --noconfirm steam

            ;;
        "Cantata")
            arch-chroot /mnt pacman -S --noconfirm cantata perl-uri mpd

            mv /mnt/etc/mpd.conf /mnt/etc/mpd.conf.default

            mkdir /mnt/home/mpd
            chown mpd:audio /mnt/home/mpd

            mkdir /mnt/home/mpd/music
            chown  -R $username:audio /mnt/home/mpd/music

            mkdir /mnt/home/mpd/playlist
            chown mpd:audio /mnt/home/mpd/playlist

            touch /mnt/home/mpd/sticker.sql
            chown mpd:audio /mnt/home/mpd/sticker.sql

            touch /mnt/home/mpd/database
            chown mpd:audio /mnt/home/mpd/database

            echo '# See: /usr/share/doc/mpd/mpdconf.example

music_directory "/home/mpd/music"
playlist_directory "/home/mpd/playlist"
db_file "/home/mpd/database"
log_file "/home/mpd/log"
pid_file "/home/mpd/pid"
state_file "/home/mpd/state"
sticker_file "/home/mpd/sticker.sql"

audio_output {
type        "pulse"
name        "MPD Pulse Output"
server      "127.0.0.1"
}' > /mnt/etc/mpd.conf

            LineNum=$(grep -n "load-module module-native-protocol-tcp" /mnt/etc/pulse/default.pa | head -1 | cut -f1 -d:)
            LineNum=$(( $LineNum + 1 ))
            sed -i "$LineNum i \load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1" /mnt/etc/pulse/default.pa

            arch-chroot /mnt systemctl enable mpd.service
            ;;
        "Virtualbox")
            arch-chroot /mnt pacman -S --noconfirm virtualbox-host-modules-arch
            arch-chroot /mnt pacman -S --noconfirm virtualbox
            arch-chroot /mnt gpasswd -a $username vboxusers
            arch-chroot /mnt for module in `ls /lib/modules/$(uname -r)/kernel/misc/{vboxdrv.ko,vboxnetadp.ko,vboxnetflt.ko,vboxpci.ko}` do ./scripts/sign-file sha1 certs/signing_key.pem certs/signing_key.x509 $module done
            ;;
        esac
    done
}

desktop_manager(){
    desktop_manager_Changes_Message="$desktop_manager_Changes_Des \n ${selected_desktops[@]}"
    if [[ ! -z ${selected_desktops[@]} ]]; then
        if (whiptail --title "$desktop_manager_Changes_Title" --yesno "$desktop_manager_Changes_Message" $lines $cols) then
            unset selected_desktops
            select_desktops
        fi
    else
    select_desktops
    fi
    main_menu
}

select_desktops(){
    selected_desktops=($(whiptail --title "$select_desktops_Title" --checklist --separate-output "$select_desktops_Des" $lines $cols 15 \
    "Gnome" "$select_desktops_Gnome_Des" off \
    "Plasma" "$select_desktops_Plasma_Des" off \
    "XFCE" "$select_desktops_XFCE_Des" off 3>&1 1>&2 2>&3))
}

install_desktops(){
    for desktop in "${selected_desktops[@]}"; do
        case $desktop in
        "Gnome")
            arch-chroot /mnt pacman -S --noconfirm \
            gnome \
            ttf-dejavu \
            gnome-builder \
            flatpak-builder \
            dconf-editor \
            evolution \
            gedit-code-assistance \
            gnome-nettool \
            gnome-recipes \
            gnome-sound-recorder \
            gnome-tweaks \
            gnome-usage \
            gnome-weather \
            nautilus-sendto \
            polari \
            sysprof \
            eolie \
            lollypop \
            gst-libav

            arch-chroot /mnt systemctl enable gdm.service
            ;;

        "Plasma")
            arch-chroot /mnt pacman -S --noconfirm phonon-qt5-vlc libx264

            arch-chroot /mnt pacman -S --noconfirm plasma packagekit-qt5

            arch-chroot /mnt systemctl enable sddm.service

            echo "[Autologin]
Relogin=false
Session=
User=

[General]
HaltCommand=
RebootCommand=

[Theme]
Current=breeze
CursorTheme=Breeze_Snow

[Users]
MaximumUid=65000
MinimumUid=1000" > /mnt/etc/sddm.conf

            # base
            arch-chroot /mnt pacman -S --noconfirm dolphin kate konsole
            arch-chroot /mnt pacman -S --noconfirm xdg-user-dirs
            arch-chroot /mnt pacman -S --noconfirm ttf-dejavu ttf-liberation

            # admin
            arch-chroot /mnt pacman -S --noconfirm kcron cronie ksystemlog systemd-kcm
            arch-chroot /mnt pacman -S --noconfirm partitionmanager

            # graphics
            arch-chroot /mnt pacman -S --noconfirm gwenview kolourpaint okular spectacle
            arch-chroot /mnt pacman -S --noconfirm krita
            arch-chroot /mnt pacman -S --noconfirm kimageformats kipi-plugins qt5-imageformats kdegraphics-mobipocket kdegraphics-thumbnailers

            # multimedia
            arch-chroot /mnt pacman -S --noconfirm ffmpegthumbs
            arch-chroot /mnt pacman -S --noconfirm dragon kdenlive frei0r-plugins recordmydesktop


            # network
            arch-chroot /mnt pacman -S --noconfirm falkon kdenetwork-filesharing smb4k krdc krfb ktorrent

            # pim
            arch-chroot /mnt pacman -S --noconfirm \
                akonadi-calendar-tools \
                akonadiconsole \
                akregator \
                grantlee-editor \
                kaddressbook \
                kalarm \
                kdepim-addons \
                kleopatra \
                kmail \
                knotes \
                kontact \
                korganizer \
                pim-data-exporter


            # utils
            arch-chroot /mnt pacman -S --noconfirm ark filelight kcalc kcharselect kgpg kwalletmanager print-manager
            arch-chroot /mnt pacman -S --noconfirm p7zip unrar unzip zip

            # office
            arch-chroot /mnt pacman -S --noconfirm calligra
        
            # Develop
            arch-chroot /mnt pacman -S --noconfirm kdevelop cmake git
            arch-chroot /mnt pacman -S --noconfirm kdevelop-python python-pyqt5
            
            # Accessories
            arch-chroot /mnt pacman -S --noconfirm plasma5-applets-redshift-control
            ;;

        "XFCE")
            arch-chroot /mnt pacman -S --noconfirm xorg-server
            arch-chroot /mnt pacman -S --noconfirm xfce4 xfce4-goodies
            
            arch-chroot /mnt pacman -S --noconfirm lightdm lightdm-gtk-greeter
            arch-chroot /mnt systemctl enable lightdm.service
            ;;
        esac
    done
}

install_base(){
	pacstrap /mnt base base-devel
	genfstab -U /mnt >> /mnt/etc/fstab

	cp "/etc/pacman.d/mirrorlist.backup" "/mnt/etc/pacman.d/mirrorlist.backup"
}

hostname_manager(){
    hostname=$(whiptail --title "$hostname_manager_Title" --inputbox "$hostname_manager_Des" $lines $cols  3>&1 1>&2 2>&3)
    main_menu
}

set_hostname(){
    echo $hostname > /mnt/etc/hostname
}


set_sudo(){
    Wheel=$(grep -n "%wheel ALL=(ALL) ALL" /mnt/etc/sudoers | cut -f1 -d:)
    sed -i "$Wheel s/^# //g" /mnt/etc/sudoers
}

set_multilib(){
    LineNum=$(grep -n "\[multilib\]" /mnt/etc/pacman.conf | cut -f1 -d:)
    sed -i "$LineNum s/^#//g" /mnt/etc/pacman.conf
    LineNum=$(( $LineNum + 1 ))
    sed -i "$LineNum s/^#//g" /mnt/etc/pacman.conf
    arch-chroot /mnt pacman -Sy
}

set_vconsole(){
    echo "loadkeys trq
setfont eurlatgr" > /mnt/etc/vconsole.conf
}

set_locale(){
    #Burasi icin menu olusturulacak.
    encoding="#en_US.UTF-8 UTF-8"
    LineNum=$(grep -n "$encoding" /mnt/etc/locale.gen | cut -f1 -d:)
    sed -i "$LineNum s/^#//g" /mnt/etc/locale.gen
    
    encoding="#tr_TR.UTF-8 UTF-8"
    LineNum=$(grep -n "$encoding" /mnt/etc/locale.gen | cut -f1 -d:)
    sed -i "$LineNum s/^#//g" /mnt/etc/locale.gen

    arch-chroot /mnt locale-gen

    echo 'LANG=tr_TR.UTF-8' > /mnt/etc/locale.conf
    ln -s -f /mnt/usr/share/zoneinfo/Turkey /mnt/etc/localtime
    arch-chroot /mnt systemctl enable systemd-timesyncd.service
    arch-chroot /mnt hwclock --systohc --utc
}

set_headphone(){
    file="/mnt/usr/share/pulseaudio/alsa-mixer/paths/analog-output-lineout.conf"
    LineNum=$(grep -n "Jack Front Headphone" $file | head -1 | cut -f1 -d:)
    LineNum=$(( $LineNum + 1 ))
    sed -i "${LineNum}s/.*/state.plugged = yes/" $file

    file="/mnt/usr/share/pulseaudio/alsa-mixer/paths/analog-output-headphones.conf"
    LineNum=$(grep -n "Element Front" $file | head -1 | cut -f1 -d:)
    LineNum=$(( $LineNum + 1 ))
    sed -i "${LineNum}s/.*/switch = off/" $file
    LineNum=$(( $LineNum + 1 ))
    sed -i "${LineNum}s/.*/volume = off/" $file

    file="/mnt/etc/pulse/default.pa"
    LineNum=$(grep -n "load-module module-udev-detect" $file | head -1 | cut -f1 -d:)
    sed -i "${LineNum}s/.*/#load-module module-udev-detect\nload-module module-udev-detect tsched=0/" $file
    
    echo "[Unit]
Description=Create alsa state file and disable automute
After=sound.target

[Service]
Type=simple
ExecStart=/usr/bin/myalsa.sh

[Install]
WantedBy=multi-user.target" > /mnt/etc/systemd/system/myalsa.service
    
    arch-chroot /mnt systemctl daemon-reload
    arch-chroot /mnt systemctl enable myalsa.service
    
    echo "#!/bin/bash

echo 0 > /sys/module/snd_hda_intel/parameters/power_save

amixer -c 0 sset 'Auto-Mute Mode' Disabled
alsactl store

sleep 2 

systemctl start alsa-restore.service

systemctl disable myalsa.service
rm /etc/systemd/system/myalsa.service
systemctl daemon-reload
rm /usr/bin/myalsa.sh" > /mnt/usr/bin/myalsa.sh

    chmod +x /mnt/usr/bin/myalsa.sh
}

set_samba(){
    arch-chroot /mnt pacman -S --noconfirm samba
    mkdir -p /mnt/var/lib/samba/usershares
    arch-chroot /mnt groupadd -r sambashare
    arch-chroot /mnt chown root:sambashare /var/lib/samba/usershares
    arch-chroot /mnt chmod 1770 /var/lib/samba/usershares

    sambaconf="/mnt/etc/samba/smb.conf"
    curl "https://git.samba.org/samba.git/?p=samba.git;a=blob_plain;f=examples/smb.conf.default;hb=HEAD" > $sambaconf
    LineNum=$(grep -n "\[global\]" $sambaconf | cut -f1 -d:)
    LineNum=$(( $LineNum + 1 ))
    sed -i "$LineNum i \   usershare owner only = yes" $sambaconf
    sed -i "$LineNum i \   usershare allow guests = yes" $sambaconf
    sed -i "$LineNum i \   usershare max shares = 100" $sambaconf
    sed -i "$LineNum i \   usershare path = /var/lib/samba/usershares" $sambaconf
    sed -i 's/^   workgroup = MYGROUP/   workgroup = WORKGROUP/' $sambaconf

    arch-chroot /mnt systemctl enable smb.service nmb.service
    
    arch-chroot /mnt gpasswd sambashare -a $username
    echo -e "$user_password\n$user_password" | arch-chroot /mnt smbpasswd -a $username
}

check_install(){
check_status="true"

if [ -z $Rankmirrors ] || [ -z $Specificmirrors ] ; then
    whiptail --title "$check_install_Title" --msgbox "$check_install_Rankmirrors_Des" $lines $cols
    check_status="false"
fi

if [[ -z $RootDisk ]]; then
    whiptail --title "$check_install_Title" --msgbox "$check_install_RootDisk_Des" $lines $cols
    check_status="false"
fi

if [[ -z $selected_boot ]]; then
    whiptail --title "$check_install_Title" --msgbox "$check_install_selected_boot_Des" $lines $cols
    check_status="false"
fi

if [[ -z $username ]]; then
    whiptail --title "$check_install_Title" --msgbox "$check_install_username_Des" $lines $cols
    check_status="false"
fi

if [[ -z $user_password ]]; then
    whiptail --title "$check_install_Title" --msgbox "$check_install_user_password_Des" $lines $cols
    check_status="false"
fi

if [[ -z $root_password ]]; then
    whiptail --title "$check_install_Title" --msgbox "$check_install_root_password_Des" $lines $cols
    check_status="false"
fi

if [[ "$check_status" = "true" ]]; then
    start_install
fi
main_menu
}

start_install(){
    message="drivers:\n ${selected_drivers[@]} \n\n programs:\n ${selected_programs[@]} \n\n $pulseaudio $networkmanager $amdgpu $cups $trim $vboxguest"
    if (whiptail --title "$start_install_Title" --yesno "$start_install_Des" $lines $cols) then
    set_mirrorlist
    install_base
    set_boot
    set_locale
    set_hostname
    set_user
    set_sudo
    set_multilib
    install_drivers
    install_desktops
    install_programs
    set_headphone
    set_samba
    else
        main_menu
    fi
}
main_menu
